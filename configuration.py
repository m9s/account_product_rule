#This file is part of Tryton.  The COPYRIGHT file at the top level of
#this repository contains the full copyright notices and license terms.
from trytond.model import ModelView, ModelSQL, ModelSingleton, fields


class Configuration(ModelSingleton, ModelSQL, ModelView):
    'Account Rule Configuration'
    _name = 'account.account.rule.configuration'
    _description = __doc__

    default_account_revenue_rule = fields.Property(
        fields.Many2One('account.account.rule', 'Default Account Revenue Rule')
        )
    default_account_expense_rule = fields.Property(
        fields.Many2One('account.account.rule', 'Default Account Expense Rule')
        )
Configuration()
