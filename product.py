#This file is part of Tryton.  The COPYRIGHT file at the top level of
#this repository contains the full copyright notices and license terms.
from trytond.model import ModelView, ModelSQL, fields
from trytond.pyson import Eval, Not, Bool, Or
from trytond.pool import Pool


class Category(ModelSQL, ModelView):
    _name = 'product.category'

    account_revenue_rule = fields.Property(fields.Many2One(
            'account.account.rule', 'Revenue Account Rule',
            domain=[('company', '=', Eval('company', 0))],
            states={
                'invisible': Not(Bool(Eval('company'))),
            }, depends=['company'],
            help='Apply this rule on account when party is customer.'))
    account_expense_rule = fields.Property(fields.Many2One(
            'account.account.rule', 'Expense Account Rule',
            domain=[('company', '=', Eval('company', 0))],
            states={
                'invisible': Not(Bool(Eval('company'))),
            }, depends=['company'],
            help='Apply this rule on account when party is supplier.'))

Category()


class Template(ModelSQL, ModelView):
    _name = 'product.template'

    account_revenue_rule = fields.Property(fields.Many2One(
            'account.account.rule', 'Revenue Account Rule',
            domain=[('company', '=', Eval('company', 0))],
            states={
                'invisible': Or(Bool(Eval('account_category')),
                            Not(Bool(Eval('company')))),
            }, help='Apply this rule on account when party is customer.',
            depends=['company', 'account_category']))
    account_expense_rule = fields.Property(fields.Many2One(
            'account.account.rule', 'Expense Account Rule',
            domain=[('company', '=', Eval('company', 0))],
            states={
                'invisible': Or(Bool(Eval('account_category')),
                            Not(Bool(Eval('company')))),
            }, help='Apply this rule on account when party is supplier.',
            depends=['company', 'account_category']))
    account_expense_rule_used = fields.Function(fields.Many2One(
            'account.account.rule', 'Account Expense Rule Used'),
            'get_rule')
    account_revenue_rule_used = fields.Function(fields.Many2One(
            'account.account.rule', 'Account Revenue Rule Used'),
            'get_rule')

    def get_account(self, ids, name):
        # Short-cut and deactivate get_account for function fields.
        # This patch avoids domain errors when copying products.
        # Additionally it is needed to short cut checks in
        #   hg.tryton.org/2.2/modules/sale/file/92461b/sale.py#l1390
        #   hg.tryton.org/2.2/modules/purchase/file/d97d85/purchase.py#l1247
        # to avoid code duplications when extending the get_invoice_line.
        return dict([[x, 1] for x in ids])

    def get_rule(self, ids, name):
        config_obj = Pool().get('account.account.rule.configuration')
        res = {}
        name = name[:-5]
        for product in self.browse(ids):
            rule = False
            if product.account_category and product.category[name]:
                rule = product.category[name].id
            elif product[name]:
                rule = product[name].id
            if not rule:
                config = config_obj.browse(config_obj.get_singleton_id())
                rule = config['default_' + name].id
            res[product.id] = rule
        return res

Template()
