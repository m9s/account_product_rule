# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
{
    'name': 'Account Product Rule',
    'name_de_DE': 'Buchhaltung Artikel Regel',
    'version': '2.2.0',
    'author': 'virtual things',
    'email': 'info@virtual-things.biz',
    'website': 'http://www.virtual-things.biz',
    'description': '''Account Rules for Products
    - Replaces the fixed accounts on products by rules for accounts
''',
    'description_de_DE': '''Kontenregeln für Artikel
    - Ersetzt die fixierten Konten in den Artikeln durch Kontenregeln
''',
    'depends': [
        'account_product'
    ],
    'xml': [
        'account.xml',
        'product.xml',
        'configuration.xml',
    ],
    'translation': [
        'locale/de_DE.po',
    ],
}
