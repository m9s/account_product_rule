#This file is part of Tryton.  The COPYRIGHT file at the top level of
#this repository contains the full copyright notices and license terms.
from trytond.model import ModelView, ModelSQL, fields
from trytond.pyson import Get, Eval, In, If
from trytond.transaction import Transaction
from trytond.pool import Pool


class AccountRule(ModelSQL, ModelView):
    'Account Rule'
    _name = 'account.account.rule'
    _description = __doc__
    name = fields.Char('Name', required=True, translate=True, loading='lazy')
    company = fields.Many2One('company.company', 'Company', required=True,
            select=1, domain=[
                ('id', If(In('company', Eval('context', {})), '=', '!='),
                    Get(Eval('context', {}), 'company', 0)),
                ])
    lines = fields.One2Many('account.account.rule.line', 'rule', 'Lines')

    def __init__(self):
        super(AccountRule, self).__init__()
        self._error_messages.update({
            'missing_matching_rule': 'Missing account rule!\n\n'
                'Can not determine an account, because there is no '
                'account rule defined for:\n\n'
                '  * product "%s",\n'
                '  * product category "%s", and \n'
                '  * accounting configuration for default account rules.\n\n'
                'Hint: Define account rules on product or set a default rule '
                'in accounting configuration for account rule.',
            'not_apply_rule': 'Can not apply account rule!\n\n'
                'No account rule found matching all criteria:\n'
                '  * fiscalyear for date: %s\n'
                '  * taxes:\n'
                '        %s\n',
            })

    def default_company(self):
        return Transaction().context.get('company') or False

    def apply(self, rule, pattern, account=False):
        '''
        Apply rule

        :param rule: a rule id or the BrowseRecord of the rule
        :param pattern: a dictonary with rule line field as key
                and match value as value
        :return: the account id to use or False
        '''
        rule_line_obj = Pool().get('account.account.rule.line')

        if not rule:
            return False

        if isinstance(rule, (int, long)) and rule:
            rule = self.browse(rule)

        pattern = pattern.copy()

        for line in rule.lines:
            if rule_line_obj.match(line, pattern):
                return rule_line_obj.get_account(line)
        return account and account.id or False

    def get_account(self, account_type, party, date, product=None, taxes=None):
        pool = Pool()
        config_obj = pool.get('account.account.rule.configuration')
        tax_obj = pool.get('account.tax')

        if isinstance(taxes, (list, tuple)):
            taxes = tax_obj.browse(taxes)

        if taxes is None:
            taxes = []

        account = False
        #TODO: this is timeline and should be moved there
        with Transaction().set_context(effective_date=date):
            invoice_line_values = {}
            if taxes:
                invoice_line_values['taxes'] = [('set', [x.id for x in taxes])]
            account_pattern = self._get_account_rule_pattern(
                party, invoice_line_values)
        if product:
            account_id = self.apply(
                getattr(product, 'account_%s_rule_used' % account_type),
                account_pattern)
            account = self._lookup_account(account_id, date=date)

        if not account:
            config = config_obj.browse(config_obj.get_singleton_id())
            account_rule = getattr(
                config, 'default_account_%s_rule' % account_type)
            if not account_rule:
                product_name = ''
                category_name = ''
                if product:
                    product_name = product.name
                    category_name = product.category.name
                self.raise_user_error('missing_matching_rule', (
                        product_name, category_name))
            account_id = self.apply(account_rule, account_pattern)
            account = self._lookup_account(account_id, date=date)
        if not account:
            err_str = (('\n' + ' ' * 8).join([x.name or '-' for x in taxes]))
            self.raise_user_error('not_apply_rule', (date, err_str))

        return account

    def _get_account_rule_pattern(self, party, invoice_line_values):
        '''
        Get account rule pattern

        :param party: the BrowseRecord of the party
        :param invoice_line_values: a dictionary with keys and values as used
                for creating an invoice_line
        :return: a dictionary to use as pattern for account rule
        '''
        date_obj = Pool().get('ir.date')
        fiscalyear_obj = Pool().get('account.fiscalyear')

        result = {}
        if invoice_line_values.has_key('taxes'):
            result['taxes'] = invoice_line_values['taxes'][0][1]

        date = Transaction().context.get('effective_date') or date_obj.today()
        fiscalyear_ids = fiscalyear_obj.search([
                ('start_date', '<=', date),
                ('end_date', '>=', date)
                ], limit=1)
        result['fiscalyear'] = fiscalyear_ids[0]
        return result

    def _lookup_account(self, account_id, date):
        account_obj = Pool().get('account.account')

        if not account_id:
            return False
        return account_obj.browse(account_id)


AccountRule()


class AccountRuleLine(ModelSQL, ModelView):
    'Account Rule Line'
    _name = 'account.account.rule.line'
    _description = __doc__
    _rec_name = 'account'
    rule = fields.Many2One('account.account.rule', 'Rule', required=True,
            select=1, ondelete='CASCADE')
    taxes = fields.Many2Many('account.tax-account.account.rule.line',
            'line', 'tax', 'Taxes',
            domain=[
                ('company', '=', Get(Eval('_parent_rule', {}), 'company')),
                ])
    account = fields.Many2One('account.account', 'Account',
            domain=[
                ('company', '=', Get(Eval('_parent_rule', {}), 'company')),
                ('kind', '!=', 'view')
                ], required=True)
    fiscalyear = fields.Many2One('account.fiscalyear', 'Fiscalyear')
    sequence = fields.Integer('Sequence', required=True)

    def __init__(self):
        super(AccountRuleLine, self).__init__()
        self._order.insert(0, ('rule', 'ASC'))
        self._order.insert(0, ('sequence', 'ASC'))

    def default_sequence(self):
        return 10

    def match(self, line, pattern):
        '''
        Match line on pattern

        :param line: a BrowseRecord of rule line
        :param pattern: a dictonary with rule line field as key
                and match value as value
        :return: a boolean
        '''
        res = True
        for field in pattern.keys():
            if field not in self._columns:
                continue
            if self._columns[field]._type == 'many2one':
                if line[field].id != pattern[field]:
                    res = False
                    break
            elif self._columns[field]._type == 'many2many':
                record_ids = [x.id for x in line[field]]
                id_matched = False
                for record_id in record_ids:
                    if record_id in pattern[field]:
                        id_matched = True
                if not id_matched:
                    res = False
                    break
            else:
                if line[field] != pattern[field]:
                    res = False
                    break
        return res

    def get_account(self, line):
        '''
        Return list of taxes for a line

        :param line: a BrowseRecord of rule line
        :return: a list of tax id
        '''
        if line.account:
            return line.account.id
        return False

AccountRuleLine()


class AccountTaxAccountRuleLine(ModelSQL):
    'Account Tax - Account Rule Line'
    _name = 'account.tax-account.account.rule.line'
    _description = __doc__

    tax = fields.Many2One('account.tax', 'Tax', required=True,
            ondelete='RESTRICT')
    line = fields.Many2One('account.account.rule.line', 'Account Rule Line',
            required=True, ondelete='CASCADE')

AccountTaxAccountRuleLine()
